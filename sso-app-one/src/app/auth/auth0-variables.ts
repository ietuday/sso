interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'LWjFfUrjHN5SE6wxlPRVlm2e2lVLTlbv',
  domain: 'my-application.auth0.com',
  callbackURL: 'http://localhost:3000/callback'
};
